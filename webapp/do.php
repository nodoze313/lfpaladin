<?php 
  define ("CURRENT_VERSION","1.0.8");
  define ("DATABASE","lfp");
  define ("SUPPORT_EMAIL","support@pcmsite.net");
  
  
  ini_set("error_reporting",E_ALL);
  //echo "<div style='visibility: hidden;'><pre>";
  //print_r($GLOBALS);
  //echo "</pre></div>";
  include "include/classes/global.inc";
  include "include/classes/error.inc";
  include "include/classes/mysql.inc";
  include "include/classes/techs.inc";
  include "include/classes/cart.inc";
  include "include/classes/parselog.inc";
  include "include/classes/manage.inc";
  
  ini_set('session.name','PHPSESSID');
  ini_set('session.cookie_lifetime','14400');
  ini_set('session.register_globals','true');

  session_start();
  function notice($data)
  {
    echo "<div class='noformat'>".$data."</div>";
  }  
  $sql = new sql();
  if($_REQUEST['what'] == 'current') {
    if($_SESSION['what'])
      $_REQUEST['what'] = $_SESSION['what'];
    else 
      $_REQUEST['what'] = 'inventory';
  }
  
  if(!$_SESSION['techid'] && $_REQUEST['what'] != "getLogoutButton" && $_REQUEST['what'] != "log")
  {
    if($_REQUEST['what'] == "logintech")
    {
      $_SESSION['techid'] = loginTech($_REQUEST['user'],$_REQUEST['pass'],$sql);
      if(!$_SESSION['techid'])
      {
        echo "<div class='error'>Failed to login, incorrect username or password.</div>";
        include "ecomerce/login.html";
        exit;
      }
    }
    else
    {
      include "include/login.html";
      exit;
    }
  }
  
  switch($_REQUEST['what'])
  {
    //this is the updater
    case 'log':
      echo "<results>";
      decomposePackage($_REQUEST['data'],$sql);
      echo "</results>";
      if($_REQUEST['version']<CURRENT_VERSION)
      {
        sendUpdate();
      }
      break;
    
    //WebUI
    case 'logout':
      if (isset($_COOKIE[session_name()])) 
      {
        setcookie(session_name(), '', time()-42000, '/');
      }
      session_destroy();
      include "include/login.html";
      break;
    case 'checkUserExist':
      if(!checkUserExist($_REQUEST['user'],$sql) && strlen($_REQUEST['user'])>4)
      {
        echo "<span id='userAvailable' value='true'>Available</span>";
      }
      else
      {
        echo "<span id='userAvailable' value='false'>Not available</span>";
      }
      break;
    case 'getLogoutButton':
      if($_SESSION['techid'])
      {
        echo "<input type='button' value='Logout' onclick='logout();'>";
      }
      break;
    case 'adduser':
      addUser($_SESSION['techid'],$_REQUEST['user'],$_REQUEST['passa'],$sql);
      include "include/tech_main_console.php";
      break;
    case 'listusers':
      echo listUsers($_SESSION['techid'],$sql);
      break;
    case 'edituser':
      include "include/edituser.php";
      break;
    case 'deleteuser':
      if(T($_REQUEST['name']))
      {
        deleteUser($_SESSION['techid'],$_REQUEST['name'],$sql);
      }
      echo listUsers($_SESSION['techid'],$sql);
      break;
    case 'showregex':
      showRegex($_REQUEST['file'],$sql);
      break;
    case 'saveregex':
      saveRegex($_REQUEST['fileid'],$_REQUEST['guid'],$_REQUEST['regex'],$sql);
      break;
    case 'saveregexstate':
      saveRegexState($_REQUEST['fileid'],$_REQUEST['guid'],$_REQUEST['state'],$sql);
      break;
    case 'deleteregex':
      deleteRegex($_REQUEST['fileid'],$_REQUEST['regexid'],$sql);
      break;
    case 'deletefile':
      deleteFile($_SESSION['techid'],$_REQUEST['file'],$_REQUEST['user'],$sql);
      break;
    case 'settechalert':
      setTechAlert($_REQUEST['techid'],$_REQUEST['alert'],$sql);
      break;
    case 'settechemail':
      setTechEmail($_REQUEST['techid'],$_REQUEST['email'],$sql);
      break;
    case 'manage':
    default:
    include "include/tech_main_console.php";
	}
?>
