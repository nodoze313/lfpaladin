#!/bin/sh
# $1 username
#chmod 777 /home/chroot/home/$1
cd /home/$1
sudo -u $1 mkdir .ssh
cd .ssh
#make the key
ssh-keygen -q -t rsa -f /home/$1/.ssh/id_rsa -N "" -C ""
#allow the client to connect to the server
cp id_rsa.pub authorized_keys
cd ..
#let the user own thier files
chown -R $1 .ssh
#make everything readable by key readers so that php can read it but not the next door neighbor
chgrp -R keyreaders .ssh
#don't let the user change thier key
#allow key readers to read it
#don't allow others to see anything
chmod -R 500 .ssh
cat .ssh/id_rsa.pub
cat /home/chroot/home/hostkey.pub
cp .ssh/id_rsa /var/www/html/keys/id_rsa_$1
chmod 777 /var/www/html/keys/id_rsa_$1
