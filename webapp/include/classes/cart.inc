<?php
  function lookupPrice($item,$sql)
  {
    $pq = "SELECT price FROM products WHERE itemnumber = '$item'";
    $pa = $sql->query($pq,DATABASE_NAME);
    if($pa['success'])
    {
      $price = mysql_fetch_array($pa['Rquery']);
      return $price[0];
    }
    return null;
  }
  function checkAvail($item,$requested,$sql)
  {
    $pq = "SELECT quantity FROM inventory WHERE itemnumber = '$item'";
    $pa = $sql->query($pq,DATABASE_NAME);
    if($pa['success'])
    {
      $numavail = mysql_fetch_array($pa['Rquery']);
      if($numavail[0]>$requested)
        return true;
      else
        return false;
    }
    return false;
  }
  function getWeight($item,$sql)
  {
    $pq = "SELECT weight FROM products WHERE itemnumber = '$item'";
    $pa = $sql->query($pq,DATABASE_NAME);
    if($pa['success'])
    {
      $weight = mysql_fetch_array($pa['Rquery']);
      return $weight[0];
    }
    else
      return null;
  }
  function updateItemTotal(&$cart,$item,$sql)
  {
    $unitPrice = lookupPrice($item,$sql);
    if(!$unitPrice)
    {
      return false;
    }
    $count = $cart['items'][$item]['count'];
    $cart['items'][$item]['totalPrice'] = $unitPrice*$count;
    return true;
  }
  function calculateShipping($cart,&$shipping,$sql)
  {
    if(count($cart['items']))
    {
      $count = 0;
      $weight = 0;
      $items = $cart['items'];
      foreach($items as $c)
      {
        if($c['count'])
        {
          $count++;
          $tw = getWeight($c['itemnumber'],$sql);
          if($tw)
          {
            $weight += $tw;
          }
          else
          {
            echo "<font color='red'>Error</font>";
          }
        }
      }
      return $weight * PER_LB;
    }
    else
    {
      return "0.00";
    }
  }
  function updateCartTotal(&$cart,$sql)
  {
    $total = 0;
    $items = &$cart['items'];
    foreach($items as $c)
    {
      $total+=$c['totalPrice'];
    }
    $total += calculateShipping($cart,$s,$sql);
    $cart['totalPrice'] = $total;
  }
  function addtocart(&$cart,$item,$count,$sql)
  {
    //make sure there are enough in stock
    $avail = 0;
    $is = "SELECT quantity FROM inventory WHERE itemnumber='".$item."'";
    $Ris = $sql->query($is,DATABASE_NAME);
    if($Ris['success'])
    {
      $Cis = mysql_fetch_array($Ris['Rquery']);
      $avail = $Cis[0];
    }
    if($avail<$count)
    {
      echo "<font color='red'>Over Stock</font><br>";
      $cart['items'][$item]['count'] = $avail;
    }
    else
    {
      $cart['items'][$item]['count'] = $count;
    }
    $cart['items'][$item]['id'] = $item;
    if(updateItemTotal($cart,$item,$sql))
    {
      updateCartTotal($cart,$sql);
      return true;
    }
    else
    {
      return false;
    }
  }
  function remfromcar(&$cart,$item)
  {
    $cart[$item] = null;
  }
  function chngitem(&$cart,$item,$count)
  {
    $cart['items'][$item]['count'] = $count;
    updateItemTotal($cart,$item);
  }
  function getItemName($itemid,$sql)
  {
    $pq = "SELECT name FROM products WHERE itemnumber = '$itemid'";
    $pa = $sql->query($pq,DATABASE_NAME);
    if($pa['success'])
    {
      $name = mysql_fetch_array($pa['Rquery']);
      return $name;
    }
  }
  function showCart($cart,$sql)
  {
    if(count($cart['items']))
    {
      $items = $cart['items'];
      foreach($items as $c)
      {
        $name = getItemName($c['id'],$sql);
        $price = lookupPrice($c['id'],$sql);
        if($c['count'])
        {
          echo "<tr><td>".$name[0]."</td><td>$".$price."</td><td>".$c['count']."</td><td>$".$c['totalPrice']."</td></tr>";//<td onclick=''><img src='images/trash.jpg'></td>
        }
      }
    }
    else
    {
      echo "<tr><td colspan='4'>There are no items in your cart yet.</td></tr>";
    }
  }
  function showProducts($cart, $sql)
  {
    $qp = "SELECT * FROM products";
    $qa = $sql->query($qp,DATABASE_NAME);
    if($qa['success'])
    {
      $pos = 0;
      $switch = false;
      while($prod = mysql_fetch_array($qa['Rquery']))
      {
        $avail = 0;
        $class = "";
        if($switch)
        {
          $switch = false;
          $class = "prodeven";
        }
        else
        {
          $switch = true;
          $class = "prododd";
        }
        $count = 0;
        if($cart['items'][$prod['itemnumber']])
        {
          $count = $cart['items'][$prod['itemnumber']]['count'];
        }
        
        $is = "SELECT quantity FROM inventory WHERE itemnumber='".$prod['itemnumber']."'";
        $Ris = $sql->query($is,DATABASE_NAME);
        if($Ris['success'])
        {
          $Cis = mysql_fetch_array($Ris['Rquery']);
          $avail = $Cis[0];
        }
        else
          $avail = "<font color='red'>Error</font>";
        $prod['price'] = lookupPrice($prod['itemnumber'],$sql);
        echo "<form action='#' id='item".$pos."' onsubmit='return false;'><table cellpadding='1' cellspacing='0' height='140' class='".$class."'><tr><td width='25%' align='center'><img src='thumbnails/phpThumb.php?src=../".$prod['image']."&w=160' class='prodthumb'></td><td class='prodinfo'>".$prod['name']."<br>$".$prod['price']." ea.<br>Available: ".$avail."<br>Quantity:<input type='text' size='4' maxlength='4' name='count' value='".$count."' style='padding: 5;' onblur=\"addToCart('item".$pos."',document)\"><input type='hidden' name='what' value='addtocart'><input type='hidden' name='item' value='".$prod['itemnumber']."'><br><div id='resultitem".$pos."' style='width: 170;'></div></td><td align='left'><div class='proddesc'>".$prod['description']."</div></td></tr></table></form>";
        //<div id='item".$pos."button' class='button' onclick=\"addToCart('item".$pos."');\">Update Cart</div>
        $pos++;
      }
    }
  }
?>
