<?php
class paypal
{
  var $head;
  var $body;
  var $edat;
  function paypal($API,$APIInfo,$username,$password)
  {
    $head = $this->head($username,$password);
    switch($API)
    {
      case 'DP':
        $DP = new PP_Direc();
        $this->body = $DP->PP_Direc_Req($APIInfo);
        break;
      case 'AC':
        $AC = new PP_Auth();
        $this->body = $AC->PP_Auth_Req($APIInfo);
        break;
      case 'EC':
        $EC = new PP_Express();
        switch($APIInfo['funct'])
        {
          case 'SET':
            $this->body = $EC->PP_Express_Set($APIInfo['data']);
            break;
          case 'GET':
            $this->body = $EC->PP_Express_Get($APIInfo['data']);
            break;
          case 'PAY':
            $this->body = $EC->PP_Express_Pay($APIInfo['data']);
            break;
        }
        break;
    }
  }
  function connect($key,$host)
  {
    $data = $this->head;
    $data .= $this->body;
    $this->edat =  $data;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,"https://$host");
    curl_setopt($ch,CURLOPT_SSLCERT,$key);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    curl_setopt($ch,CURLOPT_TIMEOUT,30);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
    $retdata['data'] = curl_exec($ch);
    $err = curl_error($ch);
    if(strcmp($err," ") > 10)
    {
      if(!eregi($err,"Operation timed out"))
      {
        $retdata['send'] = true;
      }
      else
      {
        echo "Error:" . $err;
        $retdata['send'] = 2;
      }
    }
    else 
    {
      $retdata['send'] = 2;
    }
    curl_close($ch);
    return $retdata;
  }
  function head($username,$password)
  {
    $this->head = "
    <?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <SOAP-ENV:Envelope
      xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\"
      xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"
      xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"
      xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\"
      SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">
      <SOAP-ENV:Header>
        <RequesterCredentials
          xmlns=\"urn:ebay:api:PayPalAPI\" 
          SOAP-ENV:mustUnderstand=\"1\">
          <Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\">
          <Username>$username</Username>
          <Password>$password</Password>
          <Subject></Subject>
        </Credentials>
      </RequesterCredentials>
    </SOAP-ENV:Header>
    ";
    return $this->head;
  }
  function RParse($data,$API,$funct=null)
  {
    $parser = xml_parser_create('UTF-8');
    xml_parse_into_struct($parser,$data,$vals,$index);
    $pData = array($vals,$index);
    xml_parser_free($parser);
    switch($API)
    {
      case 'DP':
        $DP = new PP_Direc();
        $retrun = $DP->PP_Direc_Res($pData);
        break;
      case 'EC':
        $EC = new PP_Express();
        $pData['funct'] = $funct;
        $retrun = $EC->PP_Express_Res($pData);
        break;
    }
    //change return type
    return $retrun;
  }
}
class PP_Auth
{
  function PP_Capture_Req($InformationArray)
  {
    $PAuthNum = $InformationArray['PAuthNum'];
    $total = $InformationArray['total'];
    //copmplete type can later be set to continue if a generator loop is written
    //this function lacks InvoiceID AND Note
    $xmlns = "xmlns=\"urn:ebay:apis:PayPalAPI\"";//standard inclusion
    $DC = " <SOAP-ENV:Body>
              <DoCaptureReq $xmlns>
                <DoCaptureRequest xsi:type\"ns:DoCaptureReq\">
                  <Version xsi:type=\"xsd:string\">1.0</Version>
                  <DoCaptureRequestDetails $xmlns>
                    <AuthorizationID>$PAuthNum</AuthorizationID>
                    <Amount>$total</Amount>
                    <CompleteType>Complete</CompleteType>
                  </DoCaptureRequestDetails>
                </DoCaptureRequest>
              </DoCaptureReq>
            </SOAP-ENV:Body>
          </SOAP-ENV:Envelope>
    ";
    return $DC;
  }
  function PP_Capture_Res()
  {
    
  }
  function PP_Auth_Req($TransID,$total)
  {
    $xmlns = "xmlns=\"urn:ebay:apis:PayPalAPI\"";//standard inclusion
    $DA = " <SOAP-ENV:Body>
              <DoAuthorizationReq $xmlns>							
                <DoAuthorizationReqest  xsi:type\"ns:DoCaptureReq\">
                  <Version xsi:type=\"xsd:string\">1.0</Version>
                  <DoAuthorizationRequestDetails $xmlns>
                    <TransactionID>$TransID<\TransactionID>
                    <Amount>$total<\Amount>
                  </DoAuthorizationRequestDetails>
                </DoAuthorizationRequest>
              </DoAuthorizationReq>
            </SOAP-ENV:Body>
          </SOAP-ENV:Envelope>
    ";
    return $DA;
  }
  function PP_Auth_Res()
  {
    
  }
  function PP_Void_Req($AuthID)
  {
    $xmlns = "xmlns=\"urn:ebay:apis:PayPalAPI\"";//standard inclusion
    $VR = " <SOAP-ENV:Body>
              <DoVoidReq $xmlns>
                <DoVoidRequest xsi:type\"ns:DoVoidReq\">
                  <Version xsi:type=\"xsd:string\">1.0</Version>
                  <DoVoidRequestDetails $xmlns>
                    <AuthorizationID>$AuthID</AuthorizationID>
                  </DoVoidRequestDetails>
                </DoVoidRequest>
              </DoVoidReq>
            </SOAP-ENV:Body>
          </SOAP-ENV:Envelope>
    ";
    return $VR;		
  }
  function PP_Void_Res()
  {
    
  }
  function PP_Reauth_Req($AuthID,$total)
  {
    $xmlns = "xmlns=\"urn:ebay:apis:PayPalAPI\"";//standard inclusion
    $RR = " <SOAP-ENV:Body>
              <DoReauthorizationReq $xmlns>
                <DoReauthorizationRequest xsi:type\"ns:DoVoidReq\">
                  <Version xsi:type=\"xsd:string\">1.0</Version>
                  <DoReauthorizationRequestDetails $xmlns>
                    <AuthorizationID>$AuthID</AuthorizationID>
                    <Amount>$total</Amount>
                  </DoReauthorizationRequestDetails>
                </DoReauthorizationRequest>
              </DoReauthorizationReq>
            </SOAP-ENV:Body>
          </SOAP-ENV:Envelope>
    ";
    return $RR;
  }
  function PP_Reauth_Res()
  {
    
  }
}
class PP_Direc
{
  function PP_Direc_Res($data)
  {
    $vals = $data[0];
    $loop = 0;
    while($data[0][$loop])
    {
      switch ($data[0][$loop]['tag'])
      {
        case 'TIMESTAMP':
          $TS = $vals[$loop]['value'];//timestamp
        case 'ACK':
          $ack = $vals[$loop]['value'];//ack
          break;
        case 'CORRELATIONID':
          $CID = $vals[$loop]['value'];//correlation id
          break;
        case 'AVSCODE':
          $AVS = $vals[$loop]['value'];//AVS code
          break;
        case 'CVV2CODE':
          $CVV = $vals[$loop]['value'];//CVV2 code
          break;
        case 'TRANSACTIONID':
          $TID = $vals[$loop]['value'];//Transaction ID
          break;
        case 'AMOUNT':
          $AMT = $vals[$loop]['value'];//amount of sale
          break;
        default:
          break;
      }
      $loop++;
    }
    return array("TS"=>$TS,"ack"=>$ack,"CID"=>$CID,"AVS"=>$AVS,"CVS"=>$CVV,"TID"=>$TID,"AMT"=>$AMT);
  }
  function PP_Direc_Req($IA)
  {
    $PA = $this->PaymentAction();//Payment Action Element
    $CC = $this->CreditCard($IA['CCInfo']);//Credit Card Element
    $PD = $this->PaymentDetails($IA['PDInfo']);//Payment Details Element
    $IP = $this->IPAddress($IA['IP']);//IP Address Element
    $data .= "
              <SOAP-ENV:Body>
                <DoDirectPaymentReq xmlns=\"urn:ebay:api:PayPalAPI\">
                  <DoDirectPaymentRequest xsi:type=\"ns:DoDirectPayment\">
                    <Version xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"xsd:string\">1.0</Version>
                    <DoDirectPaymentRequestDetails xmlns=\"urn:ebay:apis:eBLBaseComponents\">
                      $PA
                      $PD
                      $CC
                      $IP
                    </DoDirectPaymentRequestDetails>
                  </DoDirectPaymentRequest>
                </DoDirectPaymentReq>
              </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
    ";
    return $data;
  }
  function PaymentAction($action="Sale")
  {
    switch($action)
    {
      case 'Sale':
        $PA = "<PaymentAction>Sale</PaymentAction>";
        break;
      case 'Auth':
        $PA = "<PaymentAction>Authorization</PaymentAction>";
        break;
      default:
        $PA = "<PaymentAction>Sale</PaymentAction>";
        break;
    }
    return $PA;
  }
  function CreditCard($CCInfo)
  {
    $CCType = $CCInfo['CCType'];
    $CCNum = $CCInfo['CCNum'];
    $ExpMon = $CCInfo['ExpMon'];
    $ExpYear = $CCInfo['ExpYear'];
    $CVV = $CCInfo['CVV'];
    $owner = $this->OwnerInfo($CCInfo['OInfo']);
    $CC = " <CreditCard>
              <CreditCardType>$CCType</CreditCardType>
              <CreditCardNumber>$CCNum</CreditCardNumber>
              <ExpMonth>$ExpMon</ExpMonth>
              <ExpYear>$ExpYear</ExpYear>
              <CVV2>$CVV</CVV2>
                $owner
            </CreditCard>
    ";
    return $CC;
  }
  function OwnerInfo($OInfo)
  {
    $email = $OInfo['email'];
    $first = $OInfo['first'];
    $last = $OInfo['last'];
    $address = $this->BillAddress($OInfo['address']);
    
    $OI = " <CardOwner>
              <Payer>$email</Payer>
              <PayerName>
              <FirstName>$first</FirstName>
              <LastName>$last</LastName>
              </PayerName>
                $address
            </CardOwner>
    ";
    return $OI;
  }
  function BillAddress($BAInfo)
  {
    $streeta = $BAInfo['streeta'];
    $streetb = $BAInfo['streetb'];
    $city = $BAInfo['city'];
    $state = $BAInfo['state'];
    $country = $BAInfo['country'];
    $zip = $BAInfo['zip'];
    $BA = " <Address>
              <Street1>$streeta</Street1>
              <Street2>$streetb</Street2>
              <CityName>$city</CityName>
              <StateOrProvince>$state</StateOrProvince>
              <Country>$country</Country>
              <PostalCode>$zip</PostalCode>
            </Address>
    ";
    return $BA;
  }
  function PaymentDetails($PDInfo)
  {
    $total = $PDInfo['total'];
    $PD = " <PaymentDetails>
              <OrderTotal currencyID=\"USD\" 
              xmlns=\"urn:ebay:apis:eBLBaseComponents\">
              $total
              </OrderTotal>
            </PaymentDetails>
    ";
    return $PD;
  }
  function IPAddress($ip)
  {
    $IP = " <IPAddress>$ip</IPAddress>";
    return $IP;
  }
}
class PP_Express
{
  function PP_Express_Res($data)
  {
    $loop = 0;
    $retrun = array();
    switch($data['funct'])
    {
      case 'ECSET':
        while($data[0][$loop])
        {
          switch($data[0][$loop]['tag'])
          {
            case 'TIMESTAMP':
              $retrun['TS'] = $data[0][$loop]['value'];
              break;
            case 'ACK':
              $retrun['ack'] = $data[0][$loop]['value'];
              break;
            case 'CORRELATIONID':
              $retrun['CID'] = $data[0][$loop]['value'];
              break;
            case 'TOKEN':
              $retrun['token'] = $data[0][$loop]['value'];
              break;
            default:
              break;
          }
          $loop++;
        }
        return $retrun;
        break;
        
      case 'ECGET':
        while($data[0][$loop])
        {
          switch($data[0][$loop]['tag'])
          {
            case 'TOKEN':
              $retrun['token'] = $data[0][$loop]['value'];
              break;
            case 'PAYER':
              $retrun['P'] = $data[0][$loop]['value'];
              break;
            case 'PAYERID':
              $retrun['PID'] = $data[0][$loop]['value'];
              break;
            case 'PAYERStatus':
              $retrun['PS'] = $data[0][$loop]['value'];
              break;
            case 'FIRSTName':
              $retrun['first'] = $data[0][$loop]['value'];
              break;
            case 'LASTNAME':
              $retrun['last'] = $data[0][$loop]['value'];
              break;
            case 'PAYERCOUNTRY':
              $retrun['PC'] = $data[0][$loop]['value'];
              break;
            case 'PAYERBUSINESS':
              $retrun['PB'] = $data[0][$loop]['value'];
              break;
            case 'CUSTOM':
              $retrun['custom'] = $data[0][$loop]['value'];
              break;
            case 'INVOICEID':
              $retrun['IID'] = $data[0][$loop]['value'];
              break;
            case 'CONTACTPHONE':
              $retrun['CP'] = $data[0][$loop]['value'];
              break;
            case 'ADDRESSSTATUS':
              $retrun['AS'] = $data[0][$loop]['value'];
              break;
            case 'NAME':
              $retrun['name'] = $data[0][$loop]['value'];
              break;
            case 'STREET1':
              $retrun['streeta'] = $data[0][$loop]['value'];
              break;
            case 'STREET2':
              $retrun['streetb'] = $data[0][$loop]['value'];
              break;
            case 'CITYNAME':
              $retrun['city'] = $data[0][$loop]['value'];
              break;
            case 'STATEORPROVINCE':
              $retrun['state'] = $data[0][$loop]['value'];
              break;
            case 'POSTALCODE':
              $retrun['zip'] = $data[0][$loop]['value'];
              break;
            case 'COUNTRY':
              $retrun['country'] = $data[0][$loop]['value'];
              break;
            default:
              break;
          }
          $loop++;
        }
        return $retrun;
        break;
      case 'ECDO':
        while($data[0][$loop])
        {
          switch ($data[0][$loop]['tag'])
          {
            case 'TOKEN':
              $token = $data[0][$loop]['value'];
              break;
            case 'TRANSACTIONID':
              $TID = $data[0][$loop]['value'];
              break;
            case 'TRANSACTIONTYPE':
              $TT = $data[0][$loop]['value'];
              break;
            case 'PAYMENTTYPE':
              $PT = $data[0][$loop]['value'];
              break;
            case 'PAYMENTDATE':
              $TS = $data[0][$loop]['value'];
              break;
            case 'GROSSAMOUNT':
              $AMT = $data[0][$loop]['value'];
              break;
            case 'FEEAMOUNT':
              $FA = $data[0][$loop]['value'];
              break;
            case 'SETTLEAMOUNT':
              $SA = $data[0][$loop]['value'];
              break;
            case 'TAXAMOUNT':
              $TA = $data[0][$loop]['value'];
              break;
            case 'EXCHANGERATE':
              $ER = $data[0][$loop]['value'];
              break;
            case 'PAYMENTSTATUS':
              $PS = $data[0][$loop]['value'];
              break;
            case 'PENDINGREASON':
              $PR = $data[0][$loop]['value'];
              break;
            case 'ACK':
              $ack = $data[0][$loop]['value'];
              break;
            default:
              break;
          }
          $loop++;
        }
        return array("CID"=>$token,  //I put the token in to the CID variable
                      "TID"=>$TID,
                      "TT"=>$TT,
                      "PT"=>$PT,
                      "TS"=>$TS, //This is the Payment Date for consitancy it is now the timestamp
                      "AMT"=>$AMT,
                      "FA"=>$FA,
                      "SA"=>$SA,
                      "TA"=>$TA,
                      "ER"=>$ER,
                      "PS"=>$PS,
                      "PR"=>$PR,
                      "ack"=>$ack);
        break;
        
    }
  }
  function PP_Express_Set($data)
  {
    //this needs to be updated so that retrun url and cancle url are not static
    extract($data);
    $soap = "
      <SOAP-ENV:Body>
        <SetExpressCheckoutReq xmlns=\"urn:ebay:api:PayPalAPI\">
          <SetExpressCheckoutRequest>
            <Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">
            1.0
            </Version>
            <SetExpressCheckoutRequestDetails xmlns=\"urn:ebay:apis:eBLBaseComponents\">
              <OrderTotal currencyID=\"USD\" xmlns=\"urn:ebay:apis:eBLBaseComponents\">
                $total
              </OrderTotal>
              <ReturnURL>$returl</ReturnURL>
              <CancelURL>$canurl</CancelURL>
              <NoShipping>
                $ship
              </NoShipping>
            </SetExpressCheckoutRequestDetails>
          </SetExpressCheckoutRequest>
        </SetExpressCheckoutReq>
      </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>
    ";
    return $soap;
  }
  function PP_Express_Get($data)
  {
    $token = $data['token'];
    $soap = "	<SOAP-ENV:Body>
                <GetExpressCheckoutDetailsReq xmlns=\"urn:ebay:api:PayPalAPI\">
                  <GetExpressCheckoutDetailsRequest>
                    <Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version>
                    <Token>$token</Token>
                  </GetExpressCheckoutDetailsRequest>
                </GetExpressCheckoutDetailsReq>
              </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
    ";
    return $soap;
  }
  function PP_Express_Pay($data)
  {
    extract($data);
    $PD = $this->PaymentDetails($data['PD']);
    $soap = "	<SOAP-ENV:Body>
                <DoExpressCheckoutPaymentReq xmlns=\"urn:ebay:api:PayPalAPI\">
                  <DoExpressCheckoutPaymentRequest>
                    <Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">
                      1.0
                    </Version>
                    <DoExpressCheckoutPaymentRequestDetails xmlns=\"urn:ebay:apis:eBLBaseComponents\">
                      <Token>$token</Token>
                      <PaymentAction>$PA</PaymentAction>
                      <PayerID>$PID</PayerID>
                        $PD
                    </DoExpressCheckoutPaymentRequestDetails>
                  </DoExpressCheckoutPaymentRequest>
                </DoExpressCheckoutPaymentReq>
              </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
    ";
    return $soap;
  }
  function PaymentDetails($PDInfo)
  {
    $total = $PDInfo['total'];
    $PD = " <PaymentDetails>
              <OrderTotal currencyID=\"USD\" xmlns=\"urn:ebay:apis:eBLBaseComponents\">
              $total
              </OrderTotal>
            </PaymentDetails>
    ";
    return $PD;
  }
}
class PP_Detail
{
  
}
class PP_Mass
{
  
}
class PP_Refund
{
  
}
class PP_Search
{
  
}
class PP_Error
{
  
}
class CCValidate
{
  function TLTest($type,$number)
  {
    switch($type)
    {
      case 'Visa':
        break;
      case 'MasterCard':
        break;
      case 'Discover':
        break;
    }	
  }
}
?>