<?php

function sendUpdate()
{
  $fname = $_SERVER['DOCUMENT_ROOT']."/download/out/pcm.exe ";
  //echo $fname;
  $fp = fopen($fname,"r");
  if($fp)
  {
    $update = '';
    while(!feof($fp))
    {
      $update .= fread($fp,8192);
    }
    $cs = md5($update);
    echo "<update version='".CURRENT_VERSION."' checksum='".$cs."'>";
    echo base64_encode($update);
    echo "</update>";
  }
  else
  {
    //log error send email to me
    echo "failed to open file";
  }
}

function getUserID($username,$password,$sql)
{
  $q = "SELECT guid FROM users WHERE user = '".$username."' AND pass = SHA1('".$password."')";
  $ret = $sql->query($q,DATABASE);
  $data = mysql_fetch_array($ret['Rquery']);
  if(!$data[0])
    echo "Username or password failed to authenticate\n";
  return $data[0];
}

function getUserIDInternal($user,$sql)
{
  $q = "SELECT guid FROM users WHERE user = '".$user."'";
  $r = $sql->query($q,DATABASE);
  $d = mysql_fetch_array($r['Rquery']);
  return $d[0];
}

function getFileID($file,$sql)
{
  $q = "SELECT guid FROM file WHERE filename = '".$file."'";
  $r = $sql->query($q,DATABASE);
  $d = mysql_fetch_array($r['Rquery']);
  return $d[0];
}

function listUsers($techid,$sql)
{
  $ret = "<center>";
  $uq = "SELECT user FROM users WHERE techid = '".$techid."'";
  $ur = $sql->query($uq,DATABASE);
  while($ud = mysql_fetch_array($ur['Rquery']))
  {
    $ret .= "<div id='".$ud[0]."' value='false' class='userListItem'><div class='userTitle'>".$ud[0]."</div><div class='editButton' onclick=\"editUser('".$ud[0]."')\"><img src='images/edit.png'></div><div class='trashButton' onclick=\"deleteUsers('".$ud[0]."')\"><img src='images/gtk-delete.png'></div></div>";
  }//onclick='selectDiv(this);'
  $ret .= "</center>";
  return $ret;
}

function generatePassword()
{
	$pass = "";
	$seed = array("1","2","3","4","5","6","7","8","9","0","!","@","#","%","^","&","*","(",")","+","=","a","b","c",
		"d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","w","x","y","z","A","B","C","D","E",
		"F","G","H","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
	for($p=0;$p<10;$p++)
	{
		$x = rand(0,47);
		$pass .= $seed[$x];
	}
	return $pass;
}

function checkUserExist($username,$sql)
{
	$q = "SELECT COUNT(*) FROM users WHERE user ='$username'";
	$ret = $sql->query($q,DATABASE);
	if($ret['success'])
	{
    $data = mysql_fetch_array($ret['Rquery']);
    if($data[0])
    {
      return true;
    }
  }
  else
  {
    echo "Failed to get result from database error:" . $ret['error'];
  }
	return false;
}

function addUser($techid,$username,$password,$sql)
{
	//create accounts in database
  $guid = genGUID();
  $usr = "INSERT INTO users (user,guid,techid,pass) VALUES ('$username','$guid','$techid',SHA1('$password'))";
  $usrResult = $sql->query($usr,DATABASE);
  if(!$usrResult['success'])
  {
    echo "<div class='error'>Failed to create user entry in database:<br>".$usrResult['error']."</div>";
    return;
  }
  $year = 365*24*60*60;
  $now = time();
  $expire = $year+$now;
  $acc = "INSERT INTO account (guid,signup,expire) VALUES ('$guid','$now','$expire')";
  $accResult = $sql->query($acc,DATABASE);
  if(!$usrResult['success'])
  {
    echo "<div class='error'>Failed to create accounts entry in database</div>";
    return false;
  }
  return true;
}

function deleteUser($techid,$user,$sql)
{
  //don't delete just remove from the tech
  $q = "UPDATE users SET techid = '0' WHERE user = '".$user."' AND techid = '".$techid."'";
  $sql->query($q,DATABASE);
}

//get first messages for each user and date
function showTopMessages($techid,$sql)
{
  echo "<table width='400px' align='center'><tr><td width='50px'>User</td><td width='45px'>Date</td><td width='350px'>Message</td></tr>";
  $q = "SELECT guid,user FROM users WHERE techid = '".$techid."'";
  $r = $sql->query($q,DATABASE);
  $mn = 0;
  while($d = mysql_fetch_array($r['Rquery']) && $mn<20)
  {
    $u = "SELECT date,message FROM messages WHERE userid = '".$d[0]."'";
    $s = $sql->query($u,DATABASE);
    if($e = mysql_fetch_array($s['Rquery']))
    {
      echo "<tr><td>".$d[1]."</td><td>".$e[0]."</td><td>".$e[1]."</td></tr>";
    }
  }
  echo "</table>";
}

function showUserMessages($user,$sql)
{
  echo "<table width='400px' align='center'><tr><td width='45px'>Date</td><td width='400px'>Message</td></tr>";
  $q = "SELECT guid FROM users WHERE user = '".$user."'";
  $r = $sql->query($q,DATABASE);
  $d = mysql_fetch_array($r['Rquery']);
  $u = "SELECT date,message FROM messages WHERE userid = '".$d[0]."'";
  $s = $sql->query($u,DATABASE);
  while($e = mysql_fetch_array($s['Rquery']))
  {
    echo "<tr><td>".$e[0]."</td><td>".$e[1]."</td></tr>";
  }
  echo "</table>";
}

function getTechEmail($techid,$sql)
{
  $q = "SELECT email FROM techs WHERE guid = '".$techid."'";
  $r = $sql->query($q,DATABASE);
  $d = mysql_fetch_array($r['Rquery']);
  echo "<input type='text' id='techdefeml' value='".$d[0]."' onblur=\"saveDefEml(this,'".$techid."');\">";
}
function setTechEmail($techid,$email,$sql)
{
  $q = "UPDATE techs SET email = '".$email."' WHERE guid = '".$techid."'";
  $r = $sql->query($q,DATABASE);
}

function getTechAlert($techid,$sql)
{
  $q = "SELECT alert FROM techs WHERE guid = '".$techid."'";
  $r = $sql->query($q,DATABASE);
  $d = mysql_fetch_array($r['Rquery']);
  echo "<input type='text' id='techdefalrt' value='".$d[0]."' onblur=\"saveDefAlrt(this,'".$techid."');\">";
}
function setTechAlert($techid,$alert,$sql)
{
  $q = "UPDATE techs SET alert = '".$alert."' WHERE guid = '".$techid."'";
  $r = $sql->query($q,DATABASE);
  echo $q;
}

function showUserFiles($user,$sql)
{
  $userid = getUserIDInternal($user,$sql);
  $q = "SELECT filename,guid FROM file WHERE userid = '".$userid."'";
  $r = $sql->query($q,DATABASE);
  echo "<center>";
  $p=0;
  while($d = mysql_fetch_array($r['Rquery']))
  {
    $exist = true;
    echo "<div class='fileListItem'><div class='fileItem'><textarea rows='1' cols='19' readonly wrap='off'>".$d[0]."</textarea><img src='images/edit.png' class='editFile' onclick=\"showRegex(this,'".$d[1]."');\"><img src='images/gtk-delete.png' class='trashFile' onclick=\"deleteFile('this','".$d[1]."','".$user."')\"></div></div>";
    $p++;
  }
  if(!$p)
    echo "<br><br><div style='width: 175px;'>Please install and setup the utility to populate this list</div>";
  echo "</center>";
}

function deleteFile($techid,$file,$user,$sql)
{
  $q = "UPDATE file SET userid = '0' WHERE guid = '".$file."'";
  $sql->query($q,DATABASE);
  showUserFiles($user,$sql);
}

function showRegex($fileid,$sql)
{
  showRegexByFileId($fileid,$sql);
}
function showRegexByFileId($fileid,$sql)
{
  echo "<table align='center'><tr><td width='125px'>Expression</td><td width='16px'></td><td width='16px'></td><td width='20px'></td></tr>";
  $q = "SELECT pattern,state,guid FROM regex WHERE fileid = '".$fileid."'";
  $r = $sql->query($q,DATABASE);
  while($d = mysql_fetch_array($r['Rquery']))
  {
    $state = $d[1]==1?"green":"red";
    echo "<tr><td><input type='text' size='12' maxlength='254' id='exp".$d[2]."' value = '".$d[0]."' onclick='selectRegexBlock(this);' onchange=\"showApplyButton('".$d[2]."')\"></td><td width='16px'><div id='ab".$d[2]."' class='applybutton'><img src='images/gtk-apply.png' onclick=\"saveRegex('".$d[2]."','".$fileid."',".$d[1].");\"></div></td><td><img src='images/".$state."button.png' onclick=\"saveRegexState('".$d[2]."','".$fileid."',".$d[1].");\" class='pointer'></td><td class='pointer' onclick=\"deleteRegex('".$fileid."','".$d[2]."')\"><img src='images/gtk-delete.png'></td></tr>";
  }
  echo "<tr><td><input type='text' size='12' maxlength='254' onblur=\"saveRegex('newregex','".$fileid."',0);\" id='expnewregex' onclick='selectRegexBlock(this);'></td><td><img src='images/gtk-apply.png' onclick=\"saveRegex('newregex','".$fileid."',0);\"></td><td></td></tr></table>";
}

function saveRegexState($fileid,$regexid,$state,$sql)
{
  $regex = addslashes($regex);
  if($regexid == "newregex")
  {
    showRegexByFileId($fileid,$sql);
    return;
  }
  else
  {
    $q = "UPDATE regex SET state = '".$state."' WHERE fileid = '".$fileid."' AND guid='".$regexid."'";
  }
  $r = $sql->query($q,DATABASE);
  if($r['success'])
    showRegexByFileId($fileid,$sql);
  else
    echo "Failed to update please contact " . SUPPORT_EMAIL;
}

function saveRegex($fileid,$regexid,$regex,$sql)
{
  $regex = addslashes($regex);
  if($regexid == "newregex")
  {
    $q = "INSERT INTO regex (fileid,pattern,guid,state) VALUES ('".$fileid."','".$regex."','".genGUID()."','1')";
  }
  else
  {
    $q = "UPDATE regex SET pattern = '".$regex."' WHERE fileid = '".$fileid."' AND guid='".$regexid."'";//, state = '".$state."'
  }
  $r = $sql->query($q,DATABASE);
  if($r['success'])
    showRegexByFileId($fileid,$sql);
  else
    echo "Failed to update please contact " . SUPPORT_EMAIL;
}

function deleteregex($fileid,$regexid,$sql)
{
  $q = "DELETE FROM regex WHERE guid='".$regexid."' AND fileid='".$fileid."'";
  $r = $sql->query($q,DATABASE);
  if($r['success'])
    showRegexByFileId($fileid,$sql);
  else
    echo "Failed to delete please contact " . SUPPORT_EMAIL;
}

function showStockRegex($sql)
{
  $q = "SELECT program,regex,description FROM stockregex";
  $r = $sql->query($q,DATABASE);
  if($r['success'])
  {
    echo "<center><table class='srt'><tr class='srt'><td class='srt'>Program</td><td class='srt'>Expression</td><td class='srt'>Description</td></tr>";
    while($d = mysql_fetch_array($r['Rquery']))
    {
      echo "<tr><td class='srt'>".$d[0]."</td><td class='srt pointer' onclick=\"useStockRegex('".$d[1]."')\">".$d[1]."</td><td class='srt'>".$d[2]."</td></tr>";
    }
    echo "</table></center>";
  }
  else
    echo "Failed to get list please contact ". SUPPORT_EMAIL;
}

?>


