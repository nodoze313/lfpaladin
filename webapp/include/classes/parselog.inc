<?php
ini_set("error_reporting",E_ALL);
class logFile
{
  var $userid;
  var $username;
  var $log;
  var $filename;
  var $fileid;
  var $regex;
  var $sql;
  function __construct($username,$password,$filename,$log,$sql)
  {
    $this->sql = $sql;
    $this->username = $username;
    $this->filename = addslashes($filename);
    $this->log = $log;
    if($this->userid = getUserID($this->username,$password,$this->sql))
      $this->getFile();
    else
      echo "Failed to login user";
  }
  function doParse()
  {
    if(!$this->userid)
      return false;
    $this->loadRegex();
    $error = false;
    $tm = 0;
    $messages = array();
    $alerts = array();
    $emails = array();
    foreach($this->regex as $r)
    {
      if(ereg($r[0],$this->log))
      {
        $messages[$tm] = "The pattern ".$r[0]." was matched in log ".$this->filename.".";
        $emails[$tm] = $this->getEAT($r[2],'email');
        $alerts[$tm] = $this->getEAT($r[2],'alert');
        $tm++;
      }
    }
    if($tm)
    {
      $smessages = $this->serializeMessages($messages);
      //save messages
      $this->saveMessage($smessages);
      //send alerts
      $this->sendMessages($alerts,"This is an alert please check your messages in your console.\n");
      //send emails
      $this->sendMessages($emails,$smessages);
    }
  }
  function getFile()
  {
    $q = "SELECT guid FROM file WHERE userid = '".$this->userid."' AND filename = '".$this->filename."'";
    $ret = $this->sql->query($q,DATABASE);
    $d = mysql_fetch_array($ret['Rquery']);
    if(!$d[0])
    {
      //create new file
      $q = "INSERT INTO file (userid,filename,guid) VALUES ('".$this->userid."','".$this->filename."','".genGUID()."')";
      $ret = $this->sql->query($q,DATABASE);
    }
    $this->fileid = $d[0];
  }
  function loadRegex()
  {
    if(!$this->fileid)
      return false;
    $q = "SELECT pattern,state,guid FROM regex WHERE fileid = '".$this->fileid."'";
    $regex = $this->sql->query($q,DATABASE);
    $count = 0;
    while($r = mysql_fetch_array($regex['Rquery']))
    {
      $cleaned = stripslashes($r);
      $this->regex[$count] = $cleaned;
      $count++;
    }
  }
  //get the email/alerts address from the database
  function getEA($regexid,$type)
  {
    $q = "SELECT address,state FROM ".$type." WHERE regexid = '".$regexid."'";
    $e= $this->sql->query($q,DATABASE);
    $count = 0;
    $ret = 0;
    while($r = mysql_fetch_array($e['Rquery']))
    {
      $ret[$count] = $r;
      $count++;
    }
    return $ret;
  }
  function getEAT($techid,$type)
  {
    //do an inner join here and get the techid for the user
    $q = "SELECT alert FROM techs WHERE guid = '".$techid."'";
    $e= $this->sql->query($q,DATABASE);
    $count = 0;
    $ret = 0;
    while($r = mysql_fetch_array($e['Rquery']))
    {
      $ret[$count] = $r;
      $count++;
    }
    return $ret;
  }
  function testUniq($string,$array)
  {
    $c = 0;
    foreach($array as $b)
    {
      if(!strcmp($b[$c],$string))
      {
        return false;
      }
    }
    return true;
  }
  function sendMessages($addresses,$message)
  {
    $send = 0;
    $uniqAddr = 0;
    echo "Addresses:";
    print_r($addresses);
    echo "End Addresses";
    foreach($addresses as $a)
    {
      foreach($a as $b)
      {
        if($b[1] && testUniq($b[0]))
        {
          $send[$uniqAddr] = $b[0];
        }
      }
    }
    for($p = 0;$p < count($send);$p++)
    {
      mail($send[$p],"Found problems in logs from user ".$this->username.".","Here are the lines that contain the issues:\n".$message);
    }
  }
  
  function saveMessage($message)
  {
    if(!$this->userid)
      return false;
    $q = "INSERT INTO messages (userid,message,guid) VALUES ('".$this->userid."','".addslashes($message)."','".genGUID()."')";
    $s = $this->sql->query($q,DATABASE);
  }
  function serializeMessages($messages)
  {
    $str = "";
    for($p=0;$p<count($messages);$p++)
    {
      $str .= "\n";
      $str .= $messages[$p];
    }
    return $str;
  }
}

function decomposePackage($data,$sql)
{
  $decodedData = base64_decode($data);
  $temp = $decodedData; //save it
  //get the username
  $temp = strstr($temp,"pcmuser: ");
  $username = null;
  for($p=9;$p<25;$p++)
  {
    if($temp[$p] != "\n")
      $username .= $temp[$p];
    else
      break;
  }
  
  //lookup user get userid
  $u = "SELECT guid FROM users WHERE user = '$username'";
  $ret = $sql->query($u,DATABASE);
  $data = mysql_fetch_array($ret['Rquery']);
  $userid = $data[0];
  $u = "SELECT expire FROM account WHERE guid = '$userid'";
  $ret = $sql->query($u,DATABASE);
  $data = mysql_fetch_array($ret['Rquery']);
  if($data[0]<time())
  {
    //account has expired send notice to tech
    echo "Account has expired";
    //return;
  }
  //make sure thier account is valid
  
  //get the password
  $temp = strstr($temp,"lfppass");
  $password = null;
  for($p=9;$p<25;$p++)
  {
    if($temp[$p] != "\n")
      $password .= $temp[$p];
    else
      break;
  }
  //echo "username:".$username."\npassword:".$password."\n";
  //get the action
  $temp = strstr($temp,"action: ");
  $action = null;
  for($p=8;$p<24;$p++)
  {
    if($temp[$p] != "\n")
      $action .= $temp[$p];
    else
      break;
  }
  //echo "Action:".$action."<br>";
  $temp = strstr($temp,"-----------BeginLFPLog-------------");
  $logs = explode("-----------BeginLFPLog-------------",$temp);
  $filenames = null;
  for($lp=0;$lp<count($logs);$lp++)
  {
    $clog = $logs[$lp];
    if(strlen($clog)>1)
    {
      $clog = trim($clog);
      $clog = ltrim($clog,"-----------");
      $filename = NULL;
      for($p=0;$p<255;$p++)
      {
        if($clog[$p] != "\n")
          $filename .= $clog[$p];
        else
        {
          $clog = ltrim($clog,$filename);
          break;
        }
      }
      $filenames[$lp] = $filename;
      $clog = trim($clog);
      //if nothing in log drop it
      $plogs[$lp] = $clog;
      
    }
  }
  $ret = array();
  $count = 1;
  foreach($filenames as $f)
  {
    //echo "Filename: ".$f."\n";
    $ret[$count] = new logFile($username,$password,$f,$plogs[$count],$sql);
    $ret[$count]->doParse();
    $count++;
  }
  $fp = fopen("/tmp/file1","w");
  if($fp)
  {
    fwrite($fp,$plogs[1]);
    fclose($fp);
  }/*
  echo "Filenames:<br><pre>";
  print_r($filenames);
  echo "</pre><br><br>Files:<pre>";
  echo $plogs[1];
  echo "</pre>";*/
  return $ret;
}


?>
