<?php
//this file opens a database specifed by the configuration file and the encapsulating
//file.  Should be self explainitory.
define("sql_host","localhost");
define("sql_user","user");//phpsql
define("sql_pass","password");
define("sql_name","");

class sql
{
  var $mysql;
  var $error;
  function opendata($database)
  {
    if(!$this->mysql)
    {
      $this->mysql = mysql_connect(sql_host,sql_user,sql_pass);
      if($this->mysql)
      {
        if(!$conn = mysql_select_db(sql_name . "$database"))
        {
          $this->error = mysql_error();
        }
      }
    }
    return $this->mysql;
  }
  function cdat($kill)
  {
    if($kill)
    {
      mysql_close($this->mysql);
    }
  }
  function closedata()
  {
    $this->cdat(true);
  }
  function change($database)
  {
    if($this->mysql)
    {
      if(!$conn = mysql_select_db(sql_name . "$database"))
      {
        $this->error = mysql_error();
      }
    }
    else
    {
      $this->opendata($database);
    }
  }
  function query($query,$database,$kill=false)
  {
    $this->change($database);
    if(!$Rquery = mysql_query($query,$this->mysql))
    {
      $this->cdat($kill);
      $err['success'] = false;
      $err['error'] = mysql_error() . "Query problem";
      $err['misc'] = $query;
      return $err;
    }
    else
    {
      $this->cdat($kill);
      $err['success'] = true;
      $err['Rquery'] = $Rquery;
      $err['error'] = mysql_error();
      return $err;
    }	
  }
}
?>
