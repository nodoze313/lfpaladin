//general functions and stuff
function cleanNode(pNode)
{
  var nn = pNode.childNodes.length;
  for(var np=0;np<nn;np++)
  {
    pNode.removeChild(pNode.lastChild);
  }
}

//init page stuff
function  loadConsole()
{
  new Ajax.Updater({success: "console"},"do.php",{method: "post", parameters: "what=manage"});
  setLogout();
}

//logged state stuff
function setLogout()
{
  new Ajax.Updater({success: 'logout'},'do.php',{method: 'post', parameters: "what=getLogoutButton"});
}

function login()
{
  ps=Form.serialize('info');
  new Ajax.Updater({success: 'console'},'do.php',{method: 'post', parameters: ps});
  setTimeout('setLogout()',500);
}

function logout()
{
  new Ajax.Updater({success: 'console'},'do.php',{method: 'post', parameters: "what=logout"});
  setTimeout('setLogout()',500);
}


//new user stuff
function testNewUserForm()
{
  if((document.getElementById('passa').value == document.getElementById('passb').value) && document.getElementById('userAvailable').attributes.getNamedItem("value").value == "true")
  {
    document.getElementById('addUserButton').removeEventListener("click",testNewUserForm,false);
    document.getElementById('addUserButton').addEventListener("click",addUser,false);
    document.getElementById('addUserButton').click();
  }
  else
  {
    alert("Please correct the form before submitting");
  }
}

function makeUserTest()
{
  return "what=checkUserExist&user="+document.getElementById('username').value;
}

function addUser()
{
  ps=Form.serialize('newuser');
  new Ajax.Updater({success: 'console'},'do.php',{method: 'post', parameters: ps});
  //alert("Sent new user request, post: "+ps);
}

//user list stuff
function selectDiv(d)
{
  if(d.value == "true")
  {
    d.style.backgroundColor = "#777777";
    d.value = "false";
  }
  else
  {
    d.style.backgroundColor = "#eeeeee";
    d.value = "true";
  }
}

function clearSelectSib(d)
{
  p = d.parentNode;
  d = p.firstChild;
  if(d.nodeType == Node.ELEMENT_NODE)
  {
    d.style.backgroundColor = "#777777";
    d.value = "false";
  }
  while(d = d.nextSibling)
  {
    if(d.nodeType == Node.ELEMENT_NODE)
    {
      d.style.backgroundColor = "#777777";
      d.value = "false";
    }
  }
}

function getSelectedIds(l)
{
  var ids = [];
  if(l.hasChildNodes())
  {
    node = l.firstChild;
    if(node.nodeType == Node.ELEMENT_NODE && node.value == "true")
    {
      ids.push(node.attributes.getNamedItem("id").value);
    }
    while(node = node.nextSibling)
    {
      if(node.nodeType == Node.ELEMENT_NODE && node.value == "true")
      {
        ids.push(node.attributes.getNamedItem("id").value);
      }
    }
  }
  return ids;
}

function editUser(id)
{
  ps = "what=edituser&user="+id;
  new Ajax.Updater({success: 'console'},'do.php',{method: 'post', parameters: ps});
}

function deleteUsers(id)
{
  
  if(!confirm("Are you sure you wish to delete the selected record(s)?"))
    return;
  
  ps = "what=deleteuser&name="+id;
  new Ajax.Updater({success: 'userlist_c'},'do.php',{method: 'post', parameters: ps});
}

function saveDefAlrt(d,tid)
{
  if(d.value.length==0)
  {
    return;
  }
  ps = "what=settechalert&techid="+tid+"&alert="+d.value;
  new Ajax.Updater({success: 'hide'},'do.php',{method: 'post', parameters: ps});
}

function saveDefEml(d,tid)
{
  if(d.value.length==0)
  {
    alert("You should have a default address for this setting");
    return;
  }
  ps = "what=settechemail&techid="+tid+"&email="+d.value;
  new Ajax.Updater({success: 'hide'},'do.php',{method: 'post', parameters: ps});
}

//regex stuff
function showRegex(d,f)
{
  ps = "what=showregex&file="+f;
  new Ajax.Updater({success: 'regexlist_c'},'do.php',{method: 'post', parameters: ps});
}

function deleteFile(d,f,u)
{
  if(!confirm("Are you sure you wish to delete the selected record(s)?"))
    return;
  ps = "what=deletefile&file="+f+"&user="+u;
  new Ajax.Updater({success: 'filelist_c'},'do.php',{method: 'post', parameters: ps});
}

function saveRegex(n,f,st)
{ regex = document.getElementById("exp"+n);
  if(regex.value.length==0)
  {
    return;
  }
  ps = "what=saveregex&guid="+n+"&regex="+regex.value+"&fileid="+f;
  new Ajax.Updater({success: 'regexlist_c'},'do.php',{method: 'post', parameters: ps});
}
function saveRegexState(n,f,st)
{ regex = document.getElementById("exp"+n);
  if(regex.value.length==0)
  {
    return;
  }
  st=st?0:1;
  ps = "what=saveregexstate&guid="+n+"&state="+st+"&fileid="+f;
  new Ajax.Updater({success: 'regexlist_c'},'do.php',{method: 'post', parameters: ps});
}

function deleteRegex(fid,rid)
{
  ps = "what=deleteregex&fileid="+fid+"&regexid="+rid;
  new Ajax.Updater({success: 'regexlist_c'},'do.php',{method: 'post', parameters: ps});
}

function showStockRegex()
{
  if(document.getElementById("stockregex").style.display == "block")
  {
    document.getElementById("stockregex").style.display = "none";
  }
  else
  {
    document.getElementById("stockregex").style.display = "block";
  }
}

var currentSelectedRegexBlock = null;
function selectRegexBlock(d)
{
  if(currentSelectedRegexBlock == d)
  {
    currentSelectedRegexBlock.style.backgroundColor = "#ffffff";
    currentSelectedRegexBlock = null;
  }
  else
  if(currentSelectedRegexBlock)
  {
    currentSelectedRegexBlock.style.backgroundColor = "#ffffff";
    currentSelectedRegexBlock = null;
    d.style.backgroundColor = "#efefef";
    currentSelectedRegexBlock = d;
  }
  else
  {
    d.style.backgroundColor = "#efefef";
    currentSelectedRegexBlock = d;
  }
}
function useStockRegex(r)
{
  if(!currentSelectedRegexBlock)
  {
    alert("Please select a regex to put this in.");
  }
  else
  {
    currentSelectedRegexBlock.value = r;
  }
}

function showApplyButton(id)
{
  var d = document.getElementById("ab"+id);
  d.style.display = "inline";
}


